import json
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest

client = AcsClient("<ALIBABA_ACCESS_KEY>", '<ALIBABA_SECRET_KEY>', 'ap-southeast-1')
request = CommonRequest()
request.set_accept_format('json')
request.set_domain('iot.ap-southeast-1.aliyuncs.com')
request.set_method('POST')
request.set_protocol_type('https')
request.set_version('2018-01-20')
request.set_action_name('SetDeviceProperty')

request.add_query_param('RegionId', "ap-southeast1")
request.add_query_param('ProductKey', "<PRODUCT_KEY>")
request.add_query_param('DeviceName', "<DEVICE_NAME>")

def handler(event, context):
    event_json = json.loads(event.decode("utf-8"))
    total_count = event_json["data"]["requestParameters"]["Items"]["total_count"]
    if total_count >= 10:
        anl_res = "High Traffic Area!!!..."
    elif total_count < 10 and total_count >= 5:
        anl_res = "Medium Traffic Area!!!..."
    else:
        anl_res = "Low Traffic Area!!!..."
    
    request.add_query_param('Items',str({"anl_res":anl_res}))
    response = client.do_action(request)
    print("Total vehicles : ", total_count, "\tAnalysis_result : ", anl_res)
    return anl_res

