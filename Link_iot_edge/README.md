# Setting Alibaba Link IoT Edge on the Device

## Introduction
This document provides step by step setup of Link IoT Edge on the device.

## Pre-Requisites
- ARM Based Embedded Platform. The following are recommended:
    - Rockchip 3399ProD
    - NXP i.MX 8M Plus
- An Alibaba Cloud Service account.
- Setup Resource Access Management user for Access key and Secret key.
- **Link IoT Edge** service on Alibaba account activated.

## Overview of the document
- Creating an Edge instance on Link IoT Edge console and adding properties to the device.
- Creating a function on Function Compute and adding our lambda function.
- Installing Gateway software on the device and Deploying the Edge instance from the cloud.
- See the communication logs on the gateway device

## Creating an Edge instance on Link IoT Edge console and adding properties to the device
- Log into the Alibaba Cloud console. [Use the Alibaba Cloud Management Console](https://www.alibabacloud.com/help/en/basics-for-beginners/latest/use-the-alibaba-cloud-management-console).
- Search for **Link IoT Edge** and click on **Link IoT Edge** from search results.

    If there is any error appearing on the console, change the region to **Singapore** with the top drop-down menu. Refresh the page and search for the **Link IoT Edge** service again.
- On the **Link IoT Edge** console.
    - Click on **Edge Instances** on left side menu.
    - Click on the **Create Instance** button.
        - Provide any instance name. Preferred name would be the name of the device on which the **Link IoT Edge** is being deployed.
        - For **Gateway Products**, click **Create Gateway Product** under the dropdown menu. Provide the **Product Name**. Preferred name would be the same as the instance name. Click on the **OK** button.
        - For **Gateway Devices**, click on **Add Gateway Device**  under the dropdown menu. Provide the device name. This name is used in **IoT Platform** for getting credentials.
        - For **Instance Type**, from the dropdown menu, select **Standard Edition** and click on **OK**.
- Open Alibaba Cloud console in another tab.
    - Search for **IoT Platform** and access the service from the search results.
    - In the left-hand side bar, select **Products**.

        We can see that a new product has appeared with the name given previously.
    - Click on the newly created product.
    - On the product page, click on the **Define Feature** tab from the top menu.
    - Click on the **Edit Draft** button of the **Default Module**.
    - In this window, we have to add a feature for each of the detection parameters.
    - The table below describes the features that we have to add and their configuration.

        | Feature name | Identifier | Data type |
        | - | - | - |
        | person | person | int32 |
        | car | car | int32 |
        | bus | bus | int32 |
        | truck | truck | int32 |
        | bicycle | bicycle | int32 |
        | end_to_end_fps | end_to_end_fps | float |
        | pure_fps | pure_fps | float |
        | anl_res | anl_res | text |
    - Click on **Add Self-defined Feature**. And one by one, select **Properties**, enter the feature name, identifier, data type and select **Read/Write**. Click on **OK**.

        Leave all the other fields unchanged.

    - After adding all the features, click on the **Release online** button at the bottom. On the pop up window **Release model online?**, check the box and click on **OK** to save the changes.

## Creating a function on Function Compute and adding our lambda function
- Go to the Alibaba Cloud console, search for **Function Compute** and access it.
- Click on **Services and Functions** on the left side menu.
- Click on **Create Service**, provide a service name and click on **OK**.
- On the next window - corresponding to the service created - click on **Create Function**.
    - Select **Use Built-in Runtime**.
    - In **Basic Settings**, provide the function name.
    - In **Code**, select **Runtime** as **python3.6**.
    - Keep all the other options as default and click on **Create** at the bottom.
- On the local device, in the project folder, open the python file **Link_iot_edge/index.py**.
in an editor.
    - Update <ALIBABA_ACCESS_KEY> and <ALIBABA_SECRET_KEY> values at line number 5.

        For support on how to get those, refer to [Obtain an AccessKey pair](https://www.alibabacloud.com/help/en/basics-for-beginners/latest/obtain-an-accesskey-pair).
    - Update the <PRODUCT_KEY> and <DEVICE_NAME> values at lines number 15 and 16.

        To get these values:
        - Go to the Alibaba Cloud **IoT Platform** console.
        - Select **Devices** from the left side menu.
        - We can see the device we have created. Click on the device.
        - On the top, click on **view** next to **DeviceSecret**.
        - Copy the **device name** and **product key** values into the python file.
    - Save the script.
    - Compress the modified script in the zip format.
        ```sh
        zip -r index.zip index.py
        ```
    - In the Alibaba **Function Compute** console, navigate to the function which we created.
        - On the top side, select **Upload ZIP Package** from **Upload Code** dropdown menu.
        - Upload the zip file which we created and click **Save and Deploy**. This will load our function.
        - Select **Triggers** in the top menu and select **Create Trigger**.

            You may see **Trigger Management (URL)** instead of **Triggers**. If so, access **Trigger Management (URL)** and delete the existing trigger in the list.
        - For **Trigger Type**, type and select **EventBridge**.
            - Enter a name for the trigger and click on **OK**.
- In the top search bar of Alibaba console, search for **EventBridge** and navigate to the corresponding console.
    - On the left side menu, select **Event Buses** and click on the default event bus in the list.
        - On left side menu, select **Event Rules**.

            We can see that a new event rule has been created with the trigger name given in the **Function Compute** console.
        - Click on the event rule newly created. On the top right hand-corner, select **Edit Pattern**.
            - Copy the contents of the config file located at "Link_iot_edge/event_rule.json" in the project folder.
            - Replace the content in the **Pattern Content** with the copied configuration and click on **OK**.


## Installing Gateway software on the device and Deploying the Edge instance from the cloud
- Go to the PROJECT_ROOT_FOLDER/Link_iot_edge on the target device terminal (e.g. Rockchip RK3399 ProD or NXP Imx8MP device terminal) and execute the following command to download the Link IoT Edge package for aarch64.
```sh
wget http://link-iot-edge-packet.oss-cn-shanghai.aliyuncs.com/aarch64-linux-gnu/link-iot-edge-aarch64-v2.7.0.tar.gz
```
- Execute the following command to install the python dependencies.
```sh
python3 -m pip install ws4py paho-mqtt --index-url https://mirrors.aliyun.com/pypi/simple/
```
- Execute the following command to extract the downloaded archive to the root directory.
```sh
tar -xvf link-iot-edge-aarch64-v2.7.0.tar.gz -C /
```
- Go to Alibaba Cloud console and navigate to the **IoT Platform** console.
    - On the left side, select **Devices** and then, select the device which was created previously.
    - On the top side, click **View** next to **DeviceSecret** and copy the ProductKey, DeviceName, DeviceSecret values for further steps.
    - In the **Device Information** tab, click on **Here** next to **MQTT Connection Parameters** and copy the **mqttHostUrl** and note it down for further steps.
- To start the Link IoT Edge service, we need to provide a command in the following format.
    ```sh
    /linkedge/gateway/build/bin/linkedged <Product_key> <Device_name> <Device_secret_key> <Product_MQTT_host_url> 1883 0 backend-iot-edge-tunnel-singapore.aliyun.com 443 1
    ```
    Update the values and execute the command. You may need to run the command as the root user.
- See the status of the Link IoT Edge using the following command.
    ```sh
    /linkedge/gateway/build/bin/linkedged status
    ```

    The output of the above command will look like this:
    ```
    ----------------------------------------------------------
                    Link IoT Edge Service Status
    ----------------------------------------------------------

    [  +  ]  Service irot-service is active, pid is 1825682
    [  +  ]  Service keychain-service is active, pid is 1825683
    [  +  ]  Service config-manager is active, pid is 1825684
    [  +  ]  Service logger is active, pid is 1825702
    [  -  ]  Service redis-server is inactive
    [  +  ]  Service cloud-proxy is active, pid is 1825706
    [  +  ]  Service fota is active, pid is 1825708
    [  +  ]  Service RemoteTerminalDaemon is active, pid is 1799511
    [  +  ]  Service credential is active, pid is 1825763
    [  +  ]  Service message-router is active, pid is 1825764
    [  -  ]  Service gw-cascade is inactive
    [  +  ]  Service dimu is active, pid is 1825863
    [  +  ]  Service transparent-channel is active, pid is 1825867
    [  +  ]  Service fc-base is active, pid is 1825872
    [  +  ]  Service scene is active, pid is 1825880
    [  +  ]  Service service-monitor is active, pid is 1825886
    [  -  ]  Service dps_client is inactive
    [  -  ]  Service dps/bin/dpsd is inactive
    [  +  ]  Service ModbusDebugger is active, pid is 1825892
    [  +  ]  Service ModbusDebuggerProxy is active, pid is 1825893
    [  +  ]  Service task-dispatcher is active, pid is 1825894
    [  +  ]  Service id2teed is active, pid is 1825904
    [  +  ]  Service nginx is active, pid is 1826019
    [  +  ]  Service leda-mqtt is active, pid is 1826020
    [  +  ]  Service le-discoveryd is active, pid is 1826032
    [  +  ]  Service high-availability is active, pid is 1826033
    ```

    Services being active or inactive may change. For more details, look at [Sub-device disconnection](https://www.alibabacloud.com/help/en/link-iot-edge/latest/sub-device-disconnection).
- Navigate to the **Link IoT Edge** console on the Alibaba cloud.

    If there is any error appearing on the console, change the region to **Singapore** with the top drop-down menu. Refresh the page and search for the **Link IoT Edge** service again.
- Now the status of the gateway device should be shown online. It may take
some time for the device to show online.
- Click on the gateway device and click on the **Deploy** button on the top right hand-corner.

    This will deploy the gateway configuration to the device. You can see logs and the status of the ressources.
- To stop the Link IoT Edge service use the following command.
```sh
/linkedge/gateway/build/script/iot_gateway_stop.sh
```
You may need to run the command as the root user.

## See the communication logs on the gateway device
- After launching the end-to-end application, the IoT communication logs will be saved inside the device. We can see them using the following command.
```sh
cat /linkedge/run/logger/userlog/log.INFO
```

The communication logs will be stored in this file once the inference pipeline is up and running.
- On the Alibaba Cloud console, navigate to the **IoT Platform** console and on the left side menu, select **Device Log** under **Maintenance**.
- Select the device in the dropdown.
- In the **Message Content** column, click on **Here** to see the message content for the communication logs.