# Cloud Monitoring on Alibaba Cloud

## Introduction
The Cloud Monitor collects monitor metrics of Alibaba Cloud resources and custom metrics sent by the different devices. The Cloud Monitor service can be used to analyze the detections across all the devices in one place.

## Pre-Requisites
- ARM Based Embedded Platform. The following are recommended:
    - Rockchip 3399ProD
    - NXP i.MX 8M Plus
- USB Pen Drive Flashed with ARM System-Ready EWAOL Image with preinstalled docker.
- An Alibaba Cloud Service account.
- **Cloud Monitor**, **Function Compute** and **EventBridge** services activated for Alibaba
account.
- The inference pipeline consisting of Video capturing container, ML Inference container, Application container and Cloud container for Alibaba cloud ran at least once to push metrics to the cloud.

## Overview
- Creating the Dashboard
    - Add a monitoring chart
    - View monitoring charts
- Host Monitoring for Device Statistics
    - Install/upgrade Cloud Monitor agents
    - View monitoring charts
- What is next?

## Creating the Dashboard
Follow the steps given below to set up the cloud monitoring Dashboard.
- Search for **Cloud Monitor** on Alibaba Cloud Console and access the service.
- On the left-side navigation panel, click on **Dashboard**.
- On the **Custom Dashboards** tab, click **Add Dashboard** or the **+** icon.
    - In the **Add Dashboard Group** dialog box, enter a dashboard name.
    - Click on the **Confirm** button.

### Add a monitoring chart
- On the **Cloud Monitor** Console, choose Dashboard form the left-side navigation panel.
- Click on the newly created dashboard from the dashboard list displayed.
- On the top, click on **Add View** button.
    - Select a chart type of user preference.
    - Click on **Custom Monitoring** option.
    - Change chart name as metric name (different detection metrics and fps statistics pushed by the cloud container).
    - Select **Metric Name** from the dropdown list at bottom as <GROUP_ID>/<Metric_Name>.

        If there are no metrics visible, then re-run the ARM-CSP inference pipeline along with the Cloud container for Alibaba once. This will create the new group id and it will push all the detection metrics under this group id to the cloud monitor console.

        Note that, the Group ID must be the same as the id/Number given at the time of the configuration of the cloud container for the device (Cloud_container/config/config.json) and metric name will be our detection classes and fps stats.
    - Click on **OK** to save the chart.

Optional: Adding a new device metrics to charts.
- For adding the metrics of the new device in the dashboard, on the top right corner of any chart which was created before, click on the "three bar" icon. Click on **Modify**.
- Scroll down and click on **Add Metric**. Provide the **Metric Name** under group id for the new device.
- Click on **Ok** to save the modified chart.

### View monitoring charts
Follow the steps given below in order to view a monitoring chart.
- Navigate to the **Cloud Monitor** Console, choose Dashboard form the left-side navigation panel.
- Select the custom dashboard which was created.
- Alibaba cloud provides two ways for selecting time ranges to analyze the detection charts.
    1. Predefined range (1 Hour, 6 Hours, ...).
    2. Customizable time ranges.
- Select the time range according to your needs.

## Host Monitoring for Device Statistics
Host Monitoring feature is used to collect the monitoring data of multiple operating system metrics from hosts by using the Cloud Monitor agents that are installed on hosts.

### Install/upgrade Cloud Monitor agents
- Navigate to the **Cloud Monitor** Console on Alibaba Cloud.
- In the left-side navigation pane, click **Host Monitoring**.
- On the **Host Monitoring** page, select the **Install/Upgrade Agent**.
    - Select the **Script Installation** tab.
    - Select **Host Type** as **Third-party-Host**, **OS** as **Linux** and then select **Yes** to **Whether the host is accessible over the Internet**.
    - Copy the content of the **Installation Command**. Save it in any temporary text file for further steps.
    - Click **OK**.
- Go to the cloned project folder.
    - Open the cloud monitor folder and open its Dockerfile in an editor.
    - Copy the **CMS_AGENT_ACCESSKEY** and **CMS_AGENT_SECRETKEY** from the previously saved command and paste those values inside the Dockerfile.
- Run the below command from the target device (e.g. Rockchip RK3399 or Imx8MP).
    - To build the docker image.
        ```sh
        docker build –t <IMAGE_NAME>:<IMAGE_TAG> .
        ```
        Users can choose <IMAGE_NAME> and <IMAGE_TAG> as preferred.
    - Run the docker file using the command below.
        ```sh
        docker run --name cloud_monitor --network=host --rm <IMAGE_NAME>:<IMAGE_TAG>
        ```

        This will start sending the statistics to the host monitoring console of the Cloud monitor service.

        The device may take a couple of minutes before creating an entry and then we can view the device statistics on the host monitoring console.
    - To stop the cloud_monitor container, execute the command below in a new terminal on the device.
        ```sh
            docker stop cloud_monitor
        ```

        This will stop the host monitoring service.

### View monitoring charts
- Navigate to the **Cloud Monitor** Console on Alibaba Cloud.
- In the left-side navigation pane, click **Host Monitoring**.
- Click the instance name of the newly created host.
- View the monitoring charts that indicate the metrics of your host.

## What is next?
Deploy the whole pipeline on Kubernetes. Refer to the README document available in the kubernetes folder.