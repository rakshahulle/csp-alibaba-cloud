# Setup of Alibaba Cloud for ARM-CSP architecture

## Introduction
This document provides the overall steps to enable Alibaba cloud services for the application microservices. It is recommended to first have a view and a try with the application microservices available at **Traffic Monitoring/Application Microservices**.

## Clone the repository
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
Follow the below sequence to setup the services and containers.

## Setup Link IoT Edge service
Follow the README document of the Link_iot_edge folder for the setup of **Link IoT Edge service**.

## Setup the Cloud monitor Dashboard on Alibaba cloud
Follow the README document of the cloud_monitor folder for setting the cloud monitor service and dashboard.

## Setup the ApsaraVideo Live service for live streaming
Follow the pdf document available at apsara_live/docs for the setup of Apsara Live service and getting the Live streaming ingest and pull URL.

## Setup the LinkVisual service (Optional)
Follow the pdf document available at a linkvisual/docs for the setup of Apsara Live service.

## Build the cloud container
Follow the README document of the cloud_container folder for building and deploying the cloud container.

## Deploying all the containers using kubernetes
Follow the README document of the kubernetes folder for deploying all the containers using kubernetes.