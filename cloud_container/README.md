
# Building and Deploying the Cloud container

## Introduction
This document guides the user to build and deploy cloud container.

## Pre-Requisites
- ARM Based Embedded Platform.
- USB Pen Drive Flashed with ARM System-Ready EWOAL Image with preinstalled
docker.
- Setup Alibaba Cloud Service Account.
- Setup Resource Access Management user for Access key and Secret key.
- Setup Link IoT Edge on target device, refer
Link_iot_edge/docs/ARM_Smart_camera_CSP_Setting_Link_IoT_Edge.pdf for
setting Link IoT Edge on device.
- Setup CloudMonitor on Alibaba Cloud, refer cloud_monitor/docs/ ARM-
Smart_camera_CSP_Alibaba_Setting_up_Cloud_Monitor.pdf
- Setup ApsaraVideo Live service
ApsaraVideo VOD service activated for Alibaba account.
- Setup Alibaba linkVisual service (Optional)

## Clone the repository
- Clone the GitLab repo using below command on target device:
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```

## Setup the ApsaraVideo Live service on Alibaba cloud
Refer to the document at apsara_live/docs for setting the ApsaraVideo live and VOD
service for Live streaming and recording.


## Setup the Alibaba LinkVisual service on Alibaba cloud
Refer to the document at linkvisual/docs for setting the Alibaba LinkVisual Service.
This step is optional because LinkVisual service is only available on Alibaba Cloud
China account.

## Setup the container
- Execute the command on the target device where repo is cloned.
```sh
$ cd cloud_container/
```
- Open the config file config/service_config.json in an editor.
  - The Device key and Product key will be the same as the one used to set
up Link IoT Edge service on the device. To find those again:
  - Go to the Alibaba cloud and search for IoT Platform on search bar.
  - Click on the IoT Platform from search results.
  - Select the preferred region and the device which was used to deploy
  - Link IoT Edge on the local device.
  - On the top, click on view next to DeviceSecret.
  - Copy the Product Key and Device name.
  - Update the copied credentials in the config file.
- For the Cloud monitor group id, assign a number.
  - Note that all the cloud monitor metrics will be pushed to Alibaba cloud
under this id. It will be used while setting up the dashboard on the Cloud
monitor console.
- For ApsaraVideo VOD region value,
  - Refer to the document at apsara_live/docs.
  - Save the config file.
- Open the config file config/alibaba_creds.json in an editor.
  - In the config file update the region, access_key and secret_key.
  - Save the file.
- In the config file config/live_recording_config.json
  - Update the app_name, domain, stream_name, transcoding_id and
max_cycle_duration.
  - app_name: The name of the application to which the live stream belongs.
  - domain: The main streaming domain.
  - stream_name: The name of the live stream.
  - transcoding_id: A code that is passed as a string. For generating the
  transcoding id, refer to the Transcoding management documentation
  - max_cycle_duration: The duration of the periodically recorded video.
  Unit: seconds. Valid values: 300 to 21600. Default value: 3600.
  - Save the file

- For Alibaba LinkVisual
  - Unzip aly.zip folder by below command from cloud_container folder
  $ unzip aly.zip
  - Go to aly/server/config/ folder from cloud_container
  - Edit file authkey.json according to configurations of triple key while
  doing Alibaba Cloud LinkVisual Set-up
    - HostName: Name of region were LinkVisual device is created
    - ProductKey: Product key
    - DeviceName: Device name
    - DeviceSecret: Device secret
  - Save the file.
  - Uncomment line 21 to 32 from Dockerfile and save the file.

## Build and deploy Alibaba cloud container
- Change the working directory to the cloud container folder
```sh
$ cd cloud_container/
```
- Run the command given below for building the cloud container
```
# Mention the image name & tag name of your choice
$ docker build -t <IMAGE_NAME>:<TAG_NAME>.

# Check the Docker Images list with the command below
$ docker images
```
- Run the cloud container using the command below
```sh
$ docker run --network=host <IMAGE_NAME>:<TAG_NAME> \
-p <CLOUD_CONTAINER_PORT> \
-ls <LIVE_STREAMING_PUSH_URL_FROM_APSARA_VIDEO_LIVE>\
-lv on (optional)
```

- To obtain the Live streaming push URL follow the steps mentioned in Setting up
ApsaraVideo VOD live and getting the RTMP URL documentation.

- Arguments
  - -p: Port number upon which the flask server should run, give same as
  application container for -csp argument.
  - -lv: For Alibaba LinkVisual video service, lv parameter will on. (Use only
  when LinkVisual service is required)
  - -ls: The generated RTMP push URL from the ApsaraVideo live. (Only one
  of video service can be possible either Alibabab LinkVisual or Alibabab
  ApsaraVideo Live)


## Web UI guide
### For ApsaraVideo Live:

- The Web UI obtained from the application container will contain the Video
- Management server button the user needs to click that button.
OR the user can open the flask url in browser to directly navigate to video
management server.
- The application and cloud container will run on different ports.
- Click on the Video Management Server to get into cloud-based video
management server.
- Note: The date and time displayed on this page will be according to the region
you select to store videos on ApsaraVideo VOD. This Date and Time will not be
same as ApsaraVideo VOD console for example if account is created for Indian
user and user is using China (Shanghai) region to store videos, on Alibaba Cloud
console of ApsaraVideo VOD user will see Indian date and time and on ARMCSP
video management server page user will see timing of China (Shanghai).
- Click on any video to play it.
- The user can get the videos uploaded in a time range by specifying start and
end date time in input box.
- The user can delete videos individually, delete according to start and end date
time, delete all previous videos (will keep the latest 25 videos).
- The user can use the time-based scheduling or object-based scheduling options
to record videos as per their preferences on time to start recording for
according to specified time range or preferred object.
- Click on the Time Schedule button, the user can enter the start date and time and
the end date and time and click on Record button. After this button is triggered,
the videos will get recorded according to the specified time interval.
- For object scheduling, click on the Object Scheduling button a drop down will
appear where the user needs to select the object name (e.g., car or person or bus
or truck etc.) according to their preferences and click on Record button. As soon
as this button is triggered, object-based recording will be started. The object-
based recording will be started for further 2 mins as soon as the object is
detected.
- The video gets saved after 4 mins when the specified task is accomplished
successfully so the user needs to wait till its completed.
- Note that the internet must be stable, and the speed should be high. Sometimes
due to low internet speed errors may occur.

### For Alibaba LinkVisual:
- The Web UI obtained from the application container will not contain any of the Video Management server.
- Live video streaming on web dashboard is Local stream (Not from Alibaba cloud
LinkVisual service)
- Alibaba Cloud Playback from Alibaba cloud can be seen on App which is created
during Alibaba Cloud LinkVisual Set-up
- Once ARM SDC pipeline is up and running with Alibaba cloud container with user
can see LinkVisual tab on mobile application
- Click on it. Now you can see live streaming.
- Note: Sometimes live streaming may fail to appear on application click on retry
button.

## What is next?
- Setup the Cloud Monitor dashboard, refer the document below,
  - Refer to the README document of the Cloud Monitor folder.