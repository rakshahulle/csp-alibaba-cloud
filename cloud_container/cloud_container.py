from src.ffmpeg_sdk import FFMPEG_SDK
from src.Alibaba_cloud_agent import AlibabaCloudAgent
from src.argconfig import ArgConf
import argparse
from flask import Flask, render_template, request, redirect, flash, jsonify
import time as time1
from datetime import datetime
import datetime
from threading import Thread
import cv2
import os
import numpy as np
import subprocess
import logging

#logging.basicConfig(filename='log/error.log',
#                    level=os.environ.get("LOGLEVEL", "CRITICAL"))

ap = argparse.ArgumentParser()
args = ArgConf(ap)

if args["linkvisual"] == "on":
    from example import img_recieved

# Create Aliababa cloud cloud_agent
cloud_agent = AlibabaCloudAgent()
app = Flask(__name__)
#log = logging.getLogger('werkzeug')
#log.disabled = True

schedule_command_flag = 0
schedule_start = 0
config_count = 0
metrics = None
time_flag = 0
start_timef = None
end_timef = None

time_schedule_btn_status = 0
object_schedule_btn_status = 0


def object_based_schedule(sel_object):
    global metrics
    global schedule_command_flag, config_count, schedule_start, person_flag, time_flag
    rec_stopped = 0
    start_rec = None
    if (config_count == 0):
        if cloud_agent.recording_client.AddLiveRecordVodConfigRequest_function() is None:
            print("trying again to reconfigure...")
            cloud_agent.recording_client.AddLiveRecordVodConfigRequest_function()
        cloud_agent.recording_client.stop_command()
        config_count = 1

    while True:
        stream_status = cloud_agent.recording_client.is_live_stream_on()
        if metrics is None:
            print("metrics is None")
            continue
        if stream_status is None:
            print("stream status is None")
            continue
        if stream_status != "online":
            print("stream is offline")
            continue

        if (metrics[sel_object] > 0 and schedule_command_flag == 0):
            print(
                "------------------Object based scheduling: started the recording----------------")
            cloud_agent.recording_client.start_command()
            schedule_command_flag = 1
            start_rec = time1.time()

        if (not (start_rec is None) and schedule_command_flag == 1):
            if time1.time() - start_rec > 120:
                cloud_agent.recording_client.stop_command()
                print(
                    "------------------Object based scheduling: stopped the recording----------------")
                schedule_command_flag = 0

    print("Object based scheduling end")


def time_based_schedule(start_record, end_record):
    seconds = ":00"
    global schedule_command_flag, config_count, schedule_start, person_flag, time_flag
    global time__flag

    if (config_count == 0):
        cloud_agent.recording_client.AddLiveRecordVodConfigRequest_function()
        cloud_agent.recording_client.stop_command()
        config_count = 1

    start_record1 = start_record.split("T")[0]
    start_record2 = start_record.split("T")[1]
    end_record1 = end_record.split("T")[0]
    end_record2 = end_record.split("T")[1]

    start_concat = start_record1+" "+start_record2+seconds
    end_concat = end_record1+" "+end_record2+seconds
    start_timef = datetime.datetime.strptime(start_concat, "%Y-%m-%d %H:%M:%S")
    end_timef = datetime.datetime.strptime(end_concat, "%Y-%m-%d %H:%M:%S")

    rec_stopped = 0
    while rec_stopped == 0:
        if cloud_agent.recording_client.is_live_stream_on() is None or cloud_agent.recording_client.is_live_stream_on() != "online":
            continue
        current_time = datetime.datetime.utcnow() + datetime.timedelta(hours=5, minutes=30)
        current_timef = current_time.strftime("%Y-%m-%d %H:%M:%S")
        current_timef = datetime.datetime.strptime(
            current_timef, "%Y-%m-%d %H:%M:%S")
        print("current time: ", current_timef)

        if (current_timef > start_timef and current_timef < end_timef and schedule_command_flag == 0):
            print(
                "------------------ Time scheduling: started the recording----------------")
            cloud_agent.recording_client.start_command()
            schedule_command_flag = 1

        if (current_timef >= end_timef and schedule_command_flag == 1):
            cloud_agent.recording_client.stop_command()
            print(
                "------------------ Time scheduling: stopped the recording----------------")
            schedule_command_flag = 0
            rec_stopped = 1


if args["rtmp_url"] and args["linkvisual"] != "on":
    command = ['ffmpeg',
               '-y',
               '-f', 'rawvideo',
               '-vcodec', 'rawvideo',
               '-pix_fmt', 'bgr24',
               '-s', "{}x{}".format(640, 480),
               '-r', str(1.0),
               '-i', '-',
               '-c:v', 'libx264',
               '-pix_fmt', 'yuv420p',
               '-preset', 'ultrafast',
               '-f', 'flv',
               args["rtmp_url"]]

    pipeline = subprocess.Popen(command, stdin=subprocess.PIPE)


@app.route('/video', methods=['POST'])
def text():
    if request.method == 'POST':
        global metrics
        metrics = request.json['cloud_metrics']
        img = np.asarray(bytearray(request.json["image"]), dtype='uint8')
        img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        img = cv2.resize(img,(640,480))
        if args["linkvisual"] == "on":
            img_recieved(img)
        if args["rtmp_url"]:
            pipeline.stdin.write(img.tobytes())
        cloud_agent.send_metrics(metrics)
        end_rep = time1.time()
        return jsonify({"message": "Sent data to cloud", "End_to_End_pipeline_time": end_rep})


@app.route('/time_schedule_btn', methods=['POST'])
def time_schedule_btn():
    global time_schedule_btn_status
    if request.method == 'POST':
        if request.form.get('time_schedule_btn_val') == 'time_schedule_btn_val':
            time_based_schedule(request.form.get(
                'record_start'), request.form.get('record_end'))
            time_schedule_btn_status = 0
        return redirect('/')


@app.route('/object_schedule_btn', methods=['POST', 'GET'])
def object_schedule_btn():
    global object_schedule_btn_status

    if request.method == 'POST':
        if request.form.get('selected_obj') == 'selected_obj':
            sel_object = request.form.get('sel_option')
            print(sel_object, "\t", type(sel_object))
            object_schedule_btn_status = 1
            rec_thread = Thread(target=object_based_schedule,
                                args=([sel_object])).start()
    return redirect('/')


# Deleteing single video from video management server and Apsara VOD
@app.route('/delete_video', methods=['POST', 'GET'])
def delete_video():
    if request.method == 'POST':
        cloud_agent.apsara_client.delete_video(request.form.get('del_video'))
        #flash("Video is deleted")
    return redirect('/')


@app.route('/selected_videos', methods=['POST', 'GET'])
def selected_videos():
    if request.method == 'POST':
        # Searching videos according to start and end date time from video management server and Apsara VOD
        if request.form.get('search_selected_videos') == "search_selected_videos":
            headings, video_table_list, len_list = cloud_agent.apsara_client.search_videos(
                request.form.get('start'), request.form.get('end'))
            if (len_list == 0):
                flash("No video found")
                return redirect('/')
            return render_template('index.html', headings=headings, data=video_table_list, len_list=len_list)

        elif request.form.get('delete_selected_videos') == "delete_selected_videos":
            headings, video_table_list, len_list = cloud_agent.apsara_client.delete_selected_videos(
                request.form.get('start'), request.form.get('end'))
            # Deleteing videos according to start and end date time from video management server and Apsara VOD
            return redirect('/')

# Deleteing previous videos (keeping latest 25 videos)from video management server and Apsara VOD


@app.route('/delete_previous_videos', methods=['POST', 'GET'])
def delete_previous_videos():
    if request.method == 'POST':
        cloud_agent.apsara_client.delete_previous_videos()
    return redirect('/')

# Displaying all videos from Apsara VOD to video management server


@app.route('/')
def index():
    global time_schedule_btn_status, object_schedule_btn_status
    headings, video_table_list, len_list = cloud_agent.apsara_client.get_saved_videos_list()
    object1 = ("person", "car", "bus", "truck", "bicycle")
    return render_template('index.html', headings=headings, data=video_table_list, len_list=len_list,  object1=object1, time_schedule_btn_status=time_schedule_btn_status, object_schedule_btn_status=object_schedule_btn_status)

if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.run(host="0.0.0.0", port=args['cloud_port'])
